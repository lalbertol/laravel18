<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = User::paginate(10);


        //return $users;
        //dd($users);

        return view('user.index', ['users' => $users]);
        //resources/views/user/index.php
        //resources/views/user/index.blade.php

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return "CRreate";
         return view('user.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->name);

        //dd($request->all());

        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));

        $user->remember_token = str_random(10);
        $user->save();

        // $user = new User();
        // $user->fill($request->all());
        // $user->save();


         return redirect('/users');


        //return "alta de usuario " . $hola;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        // $user = User::find($id);
        // if ($user == null) {
        //     abort(404,'Prohibido!!!');

        // }

         //$user = User::findOrFail($id);

        return view('user.show', [
            'user' => $user,
             ]);

        //return "SHOW $id";

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('user.edit', [
            'user' => $user,
             ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->fill($request->all());
        $user->save();

        return redirect('/users/' . $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         // $user = User::findOrFail($id);
         // $user->delete();

        User::destroy($id);

         //return redirect('/users');
         return back();

    }

     public function especial()
    {
        $users = User::where('id', '>=', 20)->where('id','<=',27)->get();

        dd($users);

        return redirect('/users');
    }

}
