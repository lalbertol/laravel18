<html>
    <head>
        <title>MVC18 Alberto - @yield('title')</title>
    </head>
    <body>
        <header>
            Cabecera para toda la aplicación
        </header>
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>
