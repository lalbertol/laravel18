<!-- <!DOCTYPE html>
<html>
<head>
    <title>Lista de usuarios</title>
</head>
<body>
    <h1>Es la lista de usuarios</h1>
    <ul>

    @if (empty($products))
        <p>No hay usuario </p>
    @else
        @foreach ($products as $product)
         <li> {{$product->name }}</li>
        @endforeach
    @endif

    </ul>
</body>
</html>
 -->

 @extends('./layouts.app')

@section('title', 'Product Index')


@section('content')

 <h1>Lista de productos</h1>
    <ul>


    @if (empty($products))
        <p>No hay usuario </p>
    @else
        @foreach ($products as $product)
         <li> {{$product->name }}</li>
        @endforeach
    @endif

    </ul>

    {{$products->render()}}
@endsection
